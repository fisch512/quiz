package com.corejsf;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.enterprise.context.*;
import javax.inject.Named;
import javax.sql.DataSource;


@Named // or @ManagedBean
@ApplicationScoped
public class DatabaseBean implements Serializable{
    @Resource(name = "jdbc/quiz")
    private DataSource quizSource;

    private ArrayList<User> users;
    private User user;
    private String names;

    public void createUser(String name, String password)
            throws SQLException, IOException
    {
        String query = SQL.getSQL("create-user");
        Connection quizConnection = quizSource.getConnection();
        try
        {
            PreparedStatement statement =
                quizConnection.prepareStatement(query);
            statement.setString(1, name);
            statement.setString(2, password);
            statement.execute();

        }
        finally
        {
            quizConnection.close();
        }
    }

    public User getUser(String name)
      throws SQLException, IOException
    {
        String query = SQL.getSQL("user-query");
        Connection quizConnection = quizSource.getConnection();
        try
        {
            PreparedStatement statement =
                quizConnection.prepareStatement(query);
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();

            return buildUser(resultSet);
        }
        finally
        {
            quizConnection.close();
        }
    }

    public ArrayList<User> getUsers() throws SQLException, IOException {
        ArrayList<User> list = new ArrayList();

        Connection quizConnection = quizSource.getConnection();
        try
        {
            Statement statement =
                quizConnection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL.getSQL("get-users"));
            while(resultSet.next())
            {
                list.add(buildUserNoNext(resultSet));
            }
        }
        finally
        {
            quizConnection.close();
        }

        return list;
    }

    public boolean verifyUser(String name, String password)
            throws SQLException, IOException
    {
        boolean bool = false;
        ArrayList<User> list = getUsers();
        for(int i = 0; i < list.size(); i++)
        {
            if(list.get(i).getName().equals(name) &&
                    list.get(i).getPassword().equals(password))
                bool = true;
        }
        return bool;
    }

    public boolean userExists(String name) throws SQLException, IOException
    {
        if(getUsers() == null || getUsers().isEmpty())
            return false;
        ArrayList<User> list = getUsers();
        boolean bool = false;
        for(int i = 0; i < list.size(); i++)
        {
            if(list.get(i).getName().equals(name))
                bool = true;
        }
        return bool;
    }
  
    public void incrementScore(String username, String category, boolean right)
            throws SQLException, IOException
    {
        String queryTrue, queryFalse;
        String sqlTrue = appToDbAbb(category, true);
        String sqlFalse = appToDbAbb(category, false);
        Connection quizConnection = quizSource.getConnection();
        queryTrue = SQL.getSQL(sqlTrue);
        queryFalse = SQL.getSQL(sqlFalse);

        try
        {
            if(right)
            {
                PreparedStatement statement1 =
                    quizConnection.prepareStatement(queryTrue);
                statement1.setString(1, username);
                statement1.executeUpdate();
                
                PreparedStatement statement2 =
                    quizConnection.prepareStatement(queryFalse);
                statement2.setString(1, username);
                statement2.executeUpdate();
            }
            else
            {
                PreparedStatement statement3 =
                    quizConnection.prepareStatement(queryFalse);
                statement3.setString(1, username);
                statement3.executeUpdate();
            }
        }
        finally
        {
            quizConnection.close();
        }
    }

    public String[] getCategories() {
        String[] array = new String[4];

        array[0] = "Skateboarding";
        array[1] = "Football";
        array[2] = "Million $ Qs";
        array[3] = "Computer Science";

        return array;
    }

    public String getNamesString() throws SQLException, IOException{
        ArrayList<User> list = getUsers();
        names = "(";
        for(int i = 0; i < list.size(); i++)
        {
            names += list.get(i).getName();
            if((list.size() - i) != 1)
                names += ", ";
        }
        names += ")";

        return names;
    }

    private User buildUser(ResultSet resultSet)
      throws SQLException {
        int[][] array = new int[4][2];
        resultSet.next();
        String name = resultSet.getString("username");
        String password = resultSet.getString("password");
        array[0][0] = resultSet.getInt(4);
        array[0][1] = resultSet.getInt(5);
        array[1][0] = resultSet.getInt(6);
        array[1][1] = resultSet.getInt(7);
        array[2][0] = resultSet.getInt(8);
        array[2][1] = resultSet.getInt(9);
        array[3][0] = resultSet.getInt(10);
        array[3][1] = resultSet.getInt(11);

        return new User(name, password, array);
    }

    private User buildUserNoNext(ResultSet resultSet)
      throws SQLException {
        int[][] array = new int[4][2];
        String name = resultSet.getString("username");
        String password = resultSet.getString("password");
        array[0][0] = resultSet.getInt(4);
        array[0][1] = resultSet.getInt(5);
        array[1][0] = resultSet.getInt(6);
        array[1][1] = resultSet.getInt(7);
        array[2][0] = resultSet.getInt(8);
        array[2][1] = resultSet.getInt(9);
        array[3][0] = resultSet.getInt(10);
        array[3][1] = resultSet.getInt(11);

        return new User(name, password, array);
    }

    //Returns the table column name
    private String appToDbAbb(String category, boolean right) {
        if(category.equalsIgnoreCase("Skateboarding"))
        {
            if(right)
                return "cc1-update";
            else return "tc1-update";
        }
        else if(category.equalsIgnoreCase("Football"))
        {
            if(right)
                return "cc2-update";
            else return "tc2-update";
        }
        else if(category.equalsIgnoreCase("Million $ Qs"))
        {
            if(right)
                return "cc3-update";
            else return "tc3-update";
        }
        else
        {
            if(right)
                return "cc4-update";
            else return "tc4-update";
        }
    }
}
