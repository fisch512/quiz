package com.corejsf;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import javax.enterprise.context.Conversation;
import javax.inject.Named;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;

@Named // or @ManagedBean
@ConversationScoped
public class QuizBean implements Serializable {
   private int currentProblem;
   private int tries;
   private String response = "",  categoryString = "",
           correctAnswer = "", currentUser = "", error = "default error";
   private CategoryBean category;
   private boolean correct;
   private boolean done = false;
   private boolean start = true;
   private String[] categoryArray = {"Skateboarding", "Football",
                                        "Million $ Qs", "Computer Science"};
   private String[] currentUserScore;
   private @Inject Conversation conversation;
   private @Inject DatabaseBean database;

   // List of questions for all of the mini quizes
   private ArrayList<ProblemBean> problems = new ArrayList<ProblemBean>(Arrays.asList(
      new ProblemBean("Skateboarding",
         "What trick is generally considered the hardest?", "Kickflip", "Ollie",
          "360 Flip", "360 Flip"),
      new ProblemBean("Skateboarding",
         "What is the world record ollie height in inches?", "38", "44.5", "51.5",
         "44.5"),
      new ProblemBean("Skateboarding",
         "Who has won the most gold medals?", "Shaun White", "Tony Hawk", "Ryan Sheckler",
         "Tony Hawk"),
      new ProblemBean("Football",
         "Who threw a record six touchdown passes in a Super Bowl?", "Steve Young",
         "Brett Farve", "Eli Manning",
         "Steve Young"),
      new ProblemBean("Football",
         "What NFL team was the first to win the Vince Lombardi trophy five times?",
          "Packers", "Colts", "49ers",
         "49ers"),
      new ProblemBean("Football",
         "What team was led to Super Bowls VII, and VIII by their no-name defense?",
          "Steelers", "Jets", "Dolphins",
         "Dolphins"),
      new ProblemBean("Million $ Qs",
         "Which king was married to Eleanor of Aquitaine?", "Henry 1", "Richard 5",
         "Henry 2",
         "Henry 2"),
      new ProblemBean("Million $ Qs",
         "Which of these U.S presidents appeared on the television series Laugh-in?",
         "Jimmy Carter", "Richard Nixon", "Gerald Ford",
         "Richard Nixon"),
      new ProblemBean("Million $ Qs",
         "What scientist first determined that human sight results from images"
         + " projected onto the retina?", "Galileo", "Newton", "Kepler",
         "Kepler"),
      new ProblemBean("Computer Science",
         "If you need to sort a very large list of integers (billions), "
         + "what efficient sorting algorithm would be best?", "Quick", "Merge", "Shell",
         "Quick"),
      new ProblemBean("Computer Science",
         "What year was Google founded?", "1995", "1998", "2001",
         "1998"),
      new ProblemBean("Computer Science",
         "What was the first full-length computer generated feature film?",
         "A Bugs Life", "Toy Story", "Antz",
         "Toy Story")));

   //When a category is selected this method creates a new CategoryBean
   //with the questions from that category.
   public String categoryAction() {
        try {
            updateCurrentUserScore();
        } catch (SQLException ex) {
            setError(ex.getSQLState() + " " + ex.toString());
            ex.printStackTrace(System.err);
            return "error";
        } catch (IOException ex) {
            setError(ex.toString());
            ex.printStackTrace(System.err);
            return "error";
        }
        start = true;
        done = false;
        String str = categoryString;
        ArrayList<ProblemBean> list = new ArrayList<ProblemBean>(3);

        response = "";

        //Find specific category questions out of the list of all questions
        for(int i = 0; i < problems.size(); i++)
        {
            if(problems.get(i).getCategory().equals(str))
                list.add(problems.get(i));
        }

        this.category = new CategoryBean(str, list);

        //Always navigates to first.xhtml
        return "quiz";
   }

   //Checks to see if the user guessed correctly
   //and sets up the program for the next question
   //or navigates to done.xhtml
   public String answerAction() {
        start = false;
        if (category.getProblem().isCorrect(response)) {
            correct = true;
            try {
                database.incrementScore(currentUser, categoryString, true);
            } catch (SQLException ex) {
                error = ex.getSQLState() + ex.toString();
                return "error";
            } catch (IOException ex) {
                error = ex.toString();
                return "error";
            }

            try {
                updateCurrentUserScore();
            } catch (SQLException ex) {
                setError(ex.getSQLState() + " " + ex.toString());
                ex.printStackTrace(System.err);
                return "error";
            } catch (IOException ex) {
                setError(ex.toString());
                ex.printStackTrace(System.err);
                return "error";
            }

            response = "";
            category.next();
            if(category.hasNext()) {
                nextProblem();
                return "quiz";
            }
            else {
                currentProblem++;
                done = true;
                return "quiz";
            }
        }
        else
        {
            response = "";
            correct = false;
            try {
                database.incrementScore(currentUser, categoryString, false);
            } catch (SQLException ex) {
                error = ex.getSQLState() + ex.toString();
                return "error";
            } catch (IOException ex) {
                error = ex.toString();
                return "error";
            }

            try {
                updateCurrentUserScore();
            } catch (SQLException ex) {
                setError(ex.getSQLState() + " " + ex.toString());
                ex.printStackTrace(System.err);
                return "error";
            } catch (IOException ex) {
                setError(ex.toString());
                ex.printStackTrace(System.err);
                return "error";
            }

            category.next();
            if (category.hasNext()) {
                nextProblem();
                return "quiz";
            }
            else {
                currentProblem++;
                 done = true;
                 return "quiz";
            }
        }
   }

   //Sets up quizbean to handle another quiz and
   //returns the user to category.xhtml
   public String startOverAction() {
       try {
            updateCurrentUserScore();
        } catch (SQLException ex) {
            setError(ex.getSQLState() + " " + ex.toString());
            ex.printStackTrace(System.err);
            return "error";
        } catch (IOException ex) {
            setError(ex.toString());
            ex.printStackTrace(System.err);
            return "error";
        }
       currentProblem = 0;
       categoryString = "";
       response = "";
       return "category";
   }

   //Refreshes debug.xhtml
   public String refreshAction() {
       return "debug";
   }

   //Returns an array of the current users scores
   //formated in the style of "correct" + "/" + "trys"
   public String[] getCurrentUserScore() {
       return currentUserScore;
   }

   //Sets the current user and starts the conversation
   public void setCurrentUser(String name) {
       currentUser = name;
       conversation.begin();
   }

   //Returns the current users name
   public String getCurrentUser() {
       return currentUser;
   }

   //Returns a User object of current user
   public User getCurrentUserObj() throws SQLException, IOException {
       return database.getUser(currentUser);
   }

   //Returns current category
   public CategoryBean getCategory() {
       return category;
   }

   //Sets the current category
   public void setCategoryString(String str) {
       categoryString = str;
   }

   public String getCategoryString() {
       return categoryString;
   }

   //Returns the category array
   public String[] getCategoryArray() {
       return categoryArray;
   }

   //Returns current question
   public String getQuestion() {
       if(category.hasNext())
           return category.getProblem().getQuestion();
       else return "error";
   }

   //Returns answer to current question
   public String getAnswer() {
       return category.getProblem().getAnswer();
   }

   public String getPrevAnswer() {
       return category.getPrevProblem().getAnswer();
   }

   //Returns the users response to the question
   public String getResponse() {
       return response;
   }

   //Sets the users response to the question
   public void setResponse(String newValue) {
       response = newValue;
   }

   //Returns current problem index
   public int getCurrentProblem() {
       return currentProblem;
   }

   //Sets current problem
   public void setCurrentProblem(int x) {
       currentProblem = x;
   }

   //Returns if the user got the last question correct
   public boolean getCorrect()
   {
       return correct;
   }

   //Returns if the user is done with the quiz
   public boolean getDone()
   {
       return done;
   }

   //Returns if the user is on the first question
   public boolean getStart()
   {
       return start;
   }

   //Returns the current error string
   public String getError() {
       return error;
   }

   //Sets the error string to be displayed in error.xhtml
   public void setError(String str) {
       error = str;
   }

   //Gets ready to move onto the next problem
   private void nextProblem() {
      this.correctAnswer = category.getProblem().getAnswer();
      currentProblem++;
      response = "";
   }

   //Updates the current users scores from the database
   private void updateCurrentUserScore() throws SQLException, IOException {
       currentUserScore = database.getUser(currentUser).getScoresString();
   }
}