package com.corejsf;

import java.io.Serializable;

public class ProblemBean implements Serializable {
   private String question;
   private String answer;
   private String[] possibleAnswers = new String[3];
   private String category;

   //ProblemBean constructor
   public ProblemBean(String category, String question, String pa1, String pa2, String pa3, String answer) {
      this.question = question;
      this.answer = answer;
      this.possibleAnswers[0] = pa1;
      this.possibleAnswers[1] = pa2;
      this.possibleAnswers[2] = pa3;
      this.category = category;
   }

   //Returns question
   public String getQuestion() {
       return question;
   }

   //Returns answer
   public String getAnswer() {
       return answer;
   }

   //Returns the array of possible answers
   public String[] getPossibleAnswers() {
       return possibleAnswers;
   }

   //Returns category
   public String getCategory() {
       return category;
   }

   //Makes it easier for users to enter the correct answer
   public boolean isCorrect(String response) {
      return response.trim().equalsIgnoreCase(answer);
   }
}