/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.corejsf;

/**
 *
 * @author Matt
 */
public class User {
    private String name, password;
    private int[][] scores = new int[4][2];

    public User(String name, String password) {
        this.name = name;
        this.password = password;

        for(int i = 0; i < 2; i++)
        {
            for(int j = 0; j < 4; j++)
            {
                scores[j][i] = 0;
            }
        }
    }

    public User(String name, String password, int[][] array)
    {
        this.name = name;
        this.password = password;

        for(int i = 0; i < 2; i++)
        {
            for(int j = 0; j < 4; j++)
            {
                scores[j][i] = array[j][i];
            }
        }
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public int getRight(int category) {
        return scores[category][0];
    }

    public void setRight(int category, int x) {
        scores[category][0] = x;
    }

    public void increaseRight(int category) {
        scores[category][0]++;
    }

    public int getTrys(int category) {
        return scores[category][1];
    }

    public void setTrys(int category, int x) {
        scores[category][1] = x;
    }

    public void increaseTrys(int category) {
        scores[category][1]++;
    }

    public int[] getAllRight() {
        int[] array = new int[4];
        for(int i = 0; i < 4; i++)
        {
            array[i] = scores[i][0];
        }

        return array;
    }

     public int[] getAllTrys() {
        int[] array = new int[4];
        for(int i = 0; i < 4; i++)
        {
            array[i] = scores[i][0];
        }

        return array;
     }

     public String[] getScoresString() {
         String[] array = new String[4];

         for(int i = 0; i < 4; i++)
         {
             array[i] = getRight(i) + "/" + getTrys(i);
         }
         return array;
     }
}
