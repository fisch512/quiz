

package com.corejsf;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Matt
 */
@Named
@RequestScoped
public class RegisterBean implements Serializable{
    private @Inject DatabaseBean database;
    private @Inject QuizBean quiz;
    private String username = "", password = "", passwordCheck = "";
    private boolean verified = true, sameName = false;

    public void setUsername(String name) {
        this.username = name;
    }

    public String getUsername() {
        return "";
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return "";
    }

    public void setPasswordCheck(String password) {
        this.passwordCheck = password;
    }

    public String getPasswordCheck() {
        return "";
    }

    //True if password verify = password
    public boolean getVerified() {
        return verified;
    }

    //True if the username already exists
    public boolean getSameName() {
        return sameName;
    }

    //Checks if username already exists and that the passwords entered match
    //If so, then adds a new user to the data base
    public String registerAction() {
        boolean bool = false;
        try {
            bool = database.userExists(username);
        } catch (SQLException ex) {
            quiz.setError(ex.getSQLState() + "  " + ex.toString());
            ex.printStackTrace(System.err);
            System.err.println("third catch block SQL");
            return "error";
        } catch (IOException ex) {
            quiz.setError(ex.toString());
            ex.printStackTrace(System.err);
            System.err.println("third catch block");
            return "error";
        }

            if (bool)
            {
                sameName = true;
                return "register";
            }
            else
            {
                if (password.equals(passwordCheck)) {
                    verified = true;
                    try {
                        database.createUser(username, password);
                    } catch (SQLException ex) {
                        quiz.setError(ex.getSQLState() + " " + ex.toString());
                        ex.printStackTrace(System.err);
                        return "error";
                    }
                    catch (Exception ex) {
                        quiz.setError("second try catch");
                        return "error";
                    }
                    return "index";
                }
                else
                {
                    verified = false;
                    return "register";
                }
            }
    }
}
