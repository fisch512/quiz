
package com.corejsf;

import java.util.ArrayList;

/**
 *
 * @author Matt
 */
public class CategoryBean {

    private String name;
    private ArrayList<ProblemBean> list;
    private int count = 0;

    //Sets up the CategoryBean
    public CategoryBean(String name, ArrayList<ProblemBean> list)
    {
        count = 0;
        this.name = name;
        this.list = (ArrayList<ProblemBean>) list.clone();
    }

    //Returns the category name
    public String getName()
    {
        return name;
    }

    public ProblemBean getProblem()
    {
        return list.get(count);
    }

    public ProblemBean getPrevProblem()
    {
        return list.get(count - 1);
    }

    //Checks to see if there are more questions
    public boolean hasNext()
    {
        if(count < (list.size()))
            return true;
        else {
            //count = 0;
            return false;
        }
    }

    //Moves on to the next problem
    public void next()
    {
            count++;
    }
}
