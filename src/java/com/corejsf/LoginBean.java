package com.corejsf;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Matt
 */
@Named
@RequestScoped
public class LoginBean implements Serializable{
    private @Inject QuizBean quiz;
    private @Inject DatabaseBean database;

    private String username = "", password = "";
    private boolean verified = true;

    public String getUsername() {
        return "";
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return "";
    }

    public void setPassword(String password) {
        this.password = password;
    }

    //Returns true if a correct username & password are used
    public boolean getVerified() {
        return verified;
    }

    //Navigates to debug.xhtml with admin user and sets current user if
    //the user is verified
    public String loginAction() {
        try
        {
            verified = database.verifyUser(username, password);
        } 
        catch (SQLException ex)
        {
            quiz.setError(ex.getSQLState() + ex.toString());
            return "error";
        }
        catch (IOException ex)
        {
            quiz.setError(ex.toString());
            return "error";
        }
        if(username.equalsIgnoreCase("admin") && password.equalsIgnoreCase("admin"))
            return "debug";
        else if(verified)
        {
            quiz.setCurrentUser(username);
            return "category";
        }
        else return "index";
    }



}
